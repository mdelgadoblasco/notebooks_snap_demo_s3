# SNAP Sentinel-3 demo: Satellite data manipulation using SNAP within Jupyter Notebook
## Sentinel-3 LST level 3 generation using Binning SNAP operator
Run the application from Binder clicking to the logo here:
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mdelgadoblasco%2Fnotebooks_snap_demo_s3/HEAD?urlpath=lab)

This has been created by Earth Observation Platform & Phi-Lab Engineering Support team (PLES team).
Any enquiry send an email to : PLES_management@rheagroup.com

This repository is licensed with : **[European Space Agency Community License – v2.4 Permissive](https://essr.esa.int/license/european-space-agency-community-license-v2-4-permissive)**

There are several notebooks which:
- main notebook: S3_lst_mosaic which generates the mosaic of Level-2 LST Sentinel-3 products using SNAP

The notebooks can be modified to compute other indices or using other bands as well as customizing EM cluster parameters.

Launch the application from the logo above and enjoy!


